﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static FrissServer.Model.DocumentConfiguration;

namespace FrissServer.Model
{
    public static class DocumentDbContextExtensions
    {
        public static IQueryable<Document> GetDocuments(this DocumentDbContext dbContext,string Username = "", int pageSize = 10, int pageNumber = 1)
        {
            // Get query from DbSet
            var query = dbContext.Documents.AsQueryable();

            // Filter by: 'username'
            if (!string.IsNullOrEmpty(Username))
                query = query.Where(item => item.UserName == Username);      

            return query;
        }
        public static async Task<int> GetMaxId(this DocumentDbContext dbContext)
            => await dbContext.Documents?.MaxAsync(item => item.DocumentID);

        public static async Task<Document> GetDocumentByIdAsync(this DocumentDbContext dbContext, int id)
            => await dbContext.Documents.FirstOrDefaultAsync(item => item.DocumentID== id);

    }

    public static class IQueryableExtensions
    {
        public static IQueryable<TModel> Paging<TModel>(this IQueryable<TModel> query, int pageSize = 0, int pageNumber = 0) where TModel : class
            => pageSize > 0 && pageNumber > 0 ? query.Skip((pageNumber - 1) * pageSize).Take(pageSize) : query;
    }
}
