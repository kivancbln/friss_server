﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FrissServer.Model
{
    public class OperationResult
    {
        public string ExceptionMessage { get; set; }
        public Object Result { get; set; }
        public OperationStatus Status { get; set; } 
    }

    public enum OperationStatus
    {
        Success=1,
        Exception=2,
        Error=3
    }
}
