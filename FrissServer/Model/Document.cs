﻿using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace FrissServer.Model
{
    public partial class Document
    {
        public int DocumentID { get; set; }
        public DateTime UploadDate { get; set; }
        public DateTime? LastAccessedDate { get; set; }
        public string UserName { get; set; }
        public long FileSize { get; set; }
        public string FileName { get; set; }
        public string FullPath { get; set; }
    }

    public class DocumentConfiguration : IEntityTypeConfiguration<Document>
    {
        public void Configure(EntityTypeBuilder<Document> builder)
        {
            builder.ToTable("tb_Documents", "dbo");

            // Set key for entity
            builder.HasKey(p => p.DocumentID);

            builder.Property(p => p.FileName).HasColumnType("nvarchar(200)").IsRequired();
            builder.Property(p => p.FileSize).HasColumnType("int").IsRequired();
            builder.Property(p => p.FullPath).HasColumnType("nvarchar(200)").IsRequired();
            builder.Property(p => p.UploadDate).HasColumnType("datetime").IsRequired();
            builder.Property(p => p.LastAccessedDate).HasColumnType("datetime");
            builder.Property(p => p.UserName).HasColumnType("nvarchar(200)").IsRequired();

            builder
                .Property(p => p.DocumentID)
                .HasColumnType("int")
                .IsRequired()
                .ValueGeneratedOnAdd()
            //.HasDefaultValueSql("NEXT VALUE FOR [Sequences].[DocumentID]")
            ;
        }

        public class DocumentDbContext : DbContext
        {
            public DocumentDbContext(DbContextOptions<DocumentDbContext> options)
                : base(options)
            {
            }

            protected override void OnModelCreating(ModelBuilder modelBuilder)
            {
                // Apply configurations for entity

                modelBuilder
                    .ApplyConfiguration(new DocumentConfiguration());

                base.OnModelCreating(modelBuilder);
            }

            public DbSet<Document> Documents { get; set; }
        }
    }

    public class DownloadDocument
    {
        public Document Document { get; set; }
        public MemoryStream Content { get; set; }
        public string ContentType { get; set; }
    }
   
}
