﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace FrissServer.Model
{
    public class PostDocumentRequest
    {
        [Key]
        public int DocumentID { get; set; }
        [Required]
        [StringLength(200)]
        public string FileName { get; set; }
        [Required]
        public int FileSize { get; set; }
        [Required]
        [StringLength(200)]
        public string FullPath { get; set; }
        [Required]
        public DateTime UploadDate { get; set; }
        public DateTime LastAccessedDate { get; set; }
        [Required]
        [StringLength(100)]
        public string UserName { get; set; }
    }



    public static class Extensions
    {
        public static Document ToEntity(this PostDocumentRequest request)
            => new Document
            {
                DocumentID = request.DocumentID,
                FileName= request.FileName,
                FileSize= request.FileSize,
                UserName = request.UserName,
                FullPath= request.FullPath,
                LastAccessedDate= request.LastAccessedDate,
                UploadDate= request.UploadDate
            };
    }
}
