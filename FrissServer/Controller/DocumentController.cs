﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Security.Claims;
using System.Threading.Tasks;
using FrissServer.Model;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using static FrissServer.Model.DocumentConfiguration;

namespace FrissServer.Controller
{
    [Route("api/[controller]")]
    [ApiController]
    public class DocumentController : ControllerBase
    {
        protected readonly ILogger Logger;
        protected readonly DocumentDbContext DbContext;
        private readonly IHostingEnvironment environment;

        public DocumentController(ILogger<DocumentController> logger, DocumentDbContext dbContext, IHostingEnvironment _environment)
        {
            Logger = logger;
            DbContext = dbContext;
            environment = _environment;
        }

        [Authorize(Policy = "User")]
        [HttpGet]
        public async Task<IActionResult> GetByUserName(int pageSize = 10, int pageNumber = 1)
        {
            Logger?.LogDebug("'{0}' has been invoked", nameof(GetByUserName));
            
            var response = new PagedResponse<Document>();
            string username = Request.HttpContext.User.Claims.FirstOrDefault(x=>x.Type== ClaimTypes.UserData)?.Value;
            try
            {
                // Get the "proposed" query from repository
                var query = DbContext.GetDocuments(username,pageSize, pageNumber);

                // Set paging values
                response.PageSize = pageSize;
                response.PageNumber = pageNumber;

                // Get the total rows
                response.DocumentsCount = await query.CountAsync();

                // Get the specific page from database
                response.Model = await query.Paging(pageSize, pageNumber).ToListAsync();

                response.Message = string.Format("Page {0} of {1}, Total of documents: {2}.", pageNumber, response.PageCount, response.DocumentsCount);

                Logger?.LogInformation("Documents have been retrieved successfully.");
            }
            catch (Exception ex)
            {
                response.DidError = true;
                response.ErrorMessage = "There was an internal error, please contact to technical support.";

                Logger?.LogCritical("There was an error on '{0}' invocation: {1}", nameof(GetByUserName), ex);
            }

            return response.ToHttpResponse();
        }

        [Authorize(Policy = "Admin")]
        [HttpPost]
        public async Task<IActionResult> UploadFile(IFormFile file)
        {
            Logger?.LogDebug("'{0}' has been invoked", nameof(UploadFile));
            string newfilepath = "";
            string UserName = Request.Headers["Username"];
            var response = new SingleResponse<int>();


            string path = environment == null || string.IsNullOrEmpty(environment.ContentRootPath) ? "" : environment.ContentRootPath;
            try
            {
                if (!Directory.Exists(path + "\\uploads\\"))
                {
                    Directory.CreateDirectory(path + "\\uploads\\");
                }
                using (FileStream filestream = System.IO.File.Create(path + "\\uploads\\" + file.FileName))
                {
                    file.CopyTo(filestream);
                    filestream.Flush();
                    newfilepath = path + "\\uploads\\" + file.FileName;
                }
                
                // Create entity from request model
                var entity = new Document { FileName = file.FileName, FileSize = file.Length, FullPath = newfilepath, UploadDate = DateTime.Now, UserName = UserName };

                // Add entity to repository
                var result = DbContext.Add(entity);

                // Save entity in database
                await DbContext.SaveChangesAsync();

                // Set the entity to response model
                response.Model = result.Entity.DocumentID;
            }
            catch (Exception ex)
            {
                response.DidError = true;
                response.ErrorMessage = "There was an internal error, please contact to technical support. " + ex.ToString();

                Logger?.LogCritical("There was an error on '{0}' invocation: {1}", nameof(UploadFile), ex);
            }

            return response.ToHttpResponse();
        }

        [Authorize(Policy = "User")]
        [HttpGet("download/{id}")]
        public async Task<IActionResult> GetDocumentDownload(int id)
        {
            
            try
            {
                var download = await DbContext.GetDocumentByIdAsync(id);
                download.LastAccessedDate = DateTime.Now;
                DbContext.Update(download);
                DbContext.SaveChanges();
                var net = new System.Net.WebClient();
                var data = net.DownloadData(download.FullPath);
                var content = new System.IO.MemoryStream(data);
                return File(content, "APPLICATION/octet-stream", download.FileName);
            }
            catch(Exception ex)
            {
                return BadRequest(ex.ToString());
            }
            
        }

        
        

    }
}
