﻿using Microsoft.AspNetCore.Authorization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace FrissServer.Authentication
{
    public class AdminRequirement : AuthorizationHandler<AdminRequirement>, IAuthorizationRequirement
    {
        protected override Task HandleRequirementAsync(AuthorizationHandlerContext context, AdminRequirement requirement)
        {
            var claims = context.User.Claims.Count();

            string adminval = context.User.Claims.FirstOrDefault(x => x.Type == ClaimTypes.Role)?.Value;

            if (string.Equals(adminval,"Admin"))
            {
                context.Succeed(requirement);
            }
            else
            {
                context.Fail();
            }

            return Task.CompletedTask;
        }
    }

    public class UserRequirement : AuthorizationHandler<UserRequirement>, IAuthorizationRequirement
    {
        protected override Task HandleRequirementAsync(AuthorizationHandlerContext context, UserRequirement requirement)
        {
            var claims = context.User.Claims.Count();

            string adminval = context.User.Claims.FirstOrDefault(x => x.Type == ClaimTypes.Role)?.Value;

            if (string.Equals(adminval, "Admin") || string.Equals(adminval,"User"))
            {
                context.Succeed(requirement);
            }
            else
            {
                context.Fail();
            }

            return Task.CompletedTask;
        }
    }
}

