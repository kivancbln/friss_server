﻿using Microsoft.AspNetCore.Authentication;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.Net.Http.Headers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text.Encodings.Web;
using System.Threading.Tasks;

namespace FrissServer.Authentication
{
    public class CustomAuthHandler : AuthenticationHandler<CustomAuthOptions>
    {
        public CustomAuthHandler(IOptionsMonitor<CustomAuthOptions> options, ILoggerFactory logger, UrlEncoder encoder, ISystemClock clock)
            : base(options, logger, encoder, clock)
        {
        }

        protected override Task<AuthenticateResult> HandleAuthenticateAsync()
        {
            string adminvalue = Request.Headers["Admin"];
            string username = Request.Headers["Username"];

            // Get Authorization header value
            if (string.IsNullOrEmpty(username))
            {
                return Task.FromResult(AuthenticateResult.Fail("Unauthorized header value"));
            }

            // Create authenticated user
            var adminclaim = new Claim(ClaimTypes.Role, string.Equals(adminvalue, "1") ? "Admin" : "User");
            var userdata = new Claim(ClaimTypes.UserData, username);

            var identities = new List<ClaimsIdentity>();
            var claimidentity = new ClaimsIdentity(new[] { adminclaim });
            var useridentity = new ClaimsIdentity(new[] { userdata });

            identities.Add(claimidentity);
            identities.Add(useridentity);
            var ticket = new AuthenticationTicket(new ClaimsPrincipal(identities), Options.Scheme);
            
            return Task.FromResult(AuthenticateResult.Success(ticket));
        }
    }
}
