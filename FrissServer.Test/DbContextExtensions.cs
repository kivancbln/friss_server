﻿using FrissServer.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using static FrissServer.Model.DocumentConfiguration;

namespace FrissServer.Tests
{
    public static class DbContextExtensions
    {
        public static void Seed(this DocumentDbContext dbContext)
        {
            dbContext.RemoveRange(dbContext.Documents);
            // Add entities for DbContext instance
            var s1 = new Document
            {
                FileName = "kivanc_bilen.pdf",
                FileSize = 22,
                FullPath = "kivanc_bilen.pdf",
                UploadDate = new DateTime(2019, 8, 8, 10, 11, 12),
                UserName = "testuser"
            };
            dbContext.Add(s1);
            var s2 = new Document
            {
                FileName = "motivation.docx",
                FileSize = 22,
                FullPath = "motivation.docx",
                UploadDate = new DateTime(2019, 8, 6, 10, 11, 12),
                UserName = "testuser2"
            };
            dbContext.Add(s2);
            dbContext.SaveChanges();
        }
    }
}
