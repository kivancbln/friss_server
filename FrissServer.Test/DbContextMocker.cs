﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using static FrissServer.Model.DocumentConfiguration;

namespace FrissServer.Tests
{
    public static class DbContextMocker
    {
        public static DocumentDbContext GetDocumentDbContext(string dbName)
        {
            // Create options for DbContext instance
            var options = new DbContextOptionsBuilder<DocumentDbContext>()
                .UseInMemoryDatabase(databaseName: dbName)
                .EnableSensitiveDataLogging()
                .Options;

            // Create instance of DbContext
            var dbContext = new DocumentDbContext(options);

            // Add entities in memory
            dbContext.Seed();

            return dbContext;
        }
    }
}
