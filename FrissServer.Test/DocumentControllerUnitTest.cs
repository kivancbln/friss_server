﻿using FrissServer.Authentication;
using FrissServer.Controller;
using FrissServer.Model;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Internal;
using Microsoft.AspNetCore.Mvc;
using Moq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Security.Principal;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace FrissServer.Tests
{
    public class DocumentControllerUnitTest
    {

        [Fact, TestPriority(1)]
        public async Task GetByUserName()
        {
            // Arrange
            var dbContext = DbContextMocker.GetDocumentDbContext(nameof(GetByUserName));
            var controller = new DocumentController(null, dbContext,null);
            var user = new ClaimsPrincipal(new ClaimsIdentity(new Claim[]
            {
                new Claim(ClaimTypes.UserData, "testuser"),
                new Claim(ClaimTypes.Role, "User,Admin")
            }, "mock"));

            controller.ControllerContext = new ControllerContext()
            {
                HttpContext = new DefaultHttpContext() { User = user }
            };
            // Act
            var response = await controller.GetByUserName() as ObjectResult;
            var value = response.Value as IPagedResponse<Document>;

            dbContext.Dispose();

            // Assert
            Assert.False(value.DidError);
            Assert.Single(value.Model);
        }

        [Fact, TestPriority(2)]
        public async Task GetByUserName_NonExistingUserName()
        {
            // Arrange
            var dbContext = DbContextMocker.GetDocumentDbContext(nameof(GetByUserName));
            var controller = new DocumentController(null, dbContext, null);

            var user = new ClaimsPrincipal(new ClaimsIdentity(new Claim[]
            {
                new Claim(ClaimTypes.UserData, "nonexisting"),
                new Claim(ClaimTypes.Role, "User,Admin")
            }, "mock"));

            controller.ControllerContext = new ControllerContext()
            {
                HttpContext = new DefaultHttpContext() { User = user }
            };
            // Act


            var response = await controller.GetByUserName() as ObjectResult;
            var value = response.Value as IPagedResponse<Document>;

            dbContext.Dispose();

            // Assert
            Assert.Empty(value.Model);
        }

        [Fact, TestPriority(3)]
        public async Task GetDocumentDownload()
        {
            // Arrange
            var dbContext = DbContextMocker.GetDocumentDbContext(nameof(GetDocumentDownload));
            var mockEnvironment = new Mock<IHostingEnvironment>();
            //...Setup the mock as needed
            mockEnvironment
                .Setup(m => m.EnvironmentName)
                .Returns("Hosting:UnitTestEnvironment");
            //...other setup for mocked IHostingEnvironment...

            var controller = new DocumentController(null, dbContext,mockEnvironment.Object);
            var id = dbContext.Documents.Max(x => x.DocumentID);
            
            // Act
            var response = await controller.GetDocumentDownload(id);
            var value = response as FileStreamResult;

            dbContext.Dispose();

            // Assert
            Assert.True(!string.IsNullOrEmpty(value.FileDownloadName));
        }

        [Fact, TestPriority(4)]
        public async Task GetDocumentDownload_CheckLastAccessedDate()
        {
            // Arrange
            var dbContext = DbContextMocker.GetDocumentDbContext(nameof(GetDocumentDownload));
            var mockEnvironment = new Mock<IHostingEnvironment>();
            //...Setup the mock as needed
            mockEnvironment
                .Setup(m => m.EnvironmentName)
                .Returns("Hosting:UnitTestEnvironment");
            //...other setup for mocked IHostingEnvironment...

            var controller = new DocumentController(null, dbContext, mockEnvironment.Object);
            var id = dbContext.Documents.Max(x=>x.DocumentID);
            var checkdate = DateTime.Now;
            // Act
            var response = await controller.GetDocumentDownload(id);
            var value = response as FileStreamResult;

            var document = await dbContext.GetDocumentByIdAsync(id);

            dbContext.Dispose();

            // Assert
            Assert.True(checkdate < document.LastAccessedDate);
        }

        [Fact, TestPriority(5)]
        public async Task UploadFile()
        {
            // Arrange
            var dbContext = DbContextMocker.GetDocumentDbContext(nameof(UploadFile));
            var mockEnvironment = new Mock<IHostingEnvironment>();
            //...Setup the mock as needed
            mockEnvironment
                .Setup(m => m.EnvironmentName)
                .Returns("Hosting:UnitTestEnvironment");
            //...other setup for mocked IHostingEnvironment...
            var controller = new DocumentController(null, dbContext, mockEnvironment.Object);
            var file = new Mock<IFormFile>();
            var sourcefile = File.OpenRead(@"kivanc_bilen.pdf");
            var ms = new MemoryStream();
            var writer = new StreamWriter(ms);
            writer.Write(sourcefile);
            writer.Flush();
            ms.Position = 0;
            var fileName = "kivanc_bilen.pdf";
            file.Setup(f => f.FileName).Returns(fileName).Verifiable();
            file.Setup(_ => _.CopyToAsync(It.IsAny<Stream>(), It.IsAny<CancellationToken>()))
                .Returns((Stream stream, CancellationToken token) => ms.CopyToAsync(stream))
                .Verifiable();

            var user = new ClaimsPrincipal(new ClaimsIdentity(new Claim[]
            {
                new Claim(ClaimTypes.UserData, "kivanc"),
                new Claim(ClaimTypes.Role, "User,Admin")
            }, "mock"));

            controller.ControllerContext = new ControllerContext()
            {
                HttpContext = new DefaultHttpContext() { User = user }
            };


            

            // Act
            
            var response = await controller.UploadFile(file.Object) as ObjectResult;
            var value = response.Value as ISingleResponse<int>;

            dbContext.Dispose();

            // Assert
            Assert.False(value.DidError);
        }

        


    }
}
