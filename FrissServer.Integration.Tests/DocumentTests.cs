﻿using Microsoft.AspNetCore.Http;
using Moq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace FrissServer.Integration.Tests
{
    public class DocumentTests : IClassFixture<TestFixture<Startup>>
    {
        private HttpClient Client;

        public DocumentTests(TestFixture<Startup> fixture)
        {
            Client = fixture.Client;
        }

        [Fact]
        public async Task TestGetByUserName()
        {
            // Arrange
            var request = "/api/Document";
            Client.DefaultRequestHeaders.Add("Username", "kivanc");
            // Act
            var response = await Client.GetAsync(request);

            // Assert
            response.EnsureSuccessStatusCode();

            response.Dispose();
            //Client.Dispose();
        }

        [Fact]
        public async Task TestUploadFile()
        {
            //Arrange
            var request = "/api/Document";
            Client.DefaultRequestHeaders.Add("Admin", "1");
            Client.DefaultRequestHeaders.Add("Username", "testuser");

            // Act
            HttpResponseMessage response = new HttpResponseMessage();
            using (var file1 = File.OpenRead(@"kivanc_bilen.pdf"))
            using (var content1 = new StreamContent(file1))
            using (var formData = new MultipartFormDataContent())
            {
                // Add file (file, field name, file name)
                formData.Add(content1, "file", "kivanc_bilen.pdf");
                response = await Client.PostAsync(request, formData);
            }

            // Assert
            response.EnsureSuccessStatusCode();
            var responseString = await response.Content.ReadAsStringAsync();

            Assert.NotEmpty(responseString);
            Assert.Equal("application/json; charset=utf-8", response.Content.Headers.ContentType.ToString());
            response.Dispose();
            //Client.Dispose();
        }

        [Fact]
        public async Task TestUploadFile_WoutAdminHeader()
        {
            //Arrange
            var request = "/api/Document";
            Client.DefaultRequestHeaders.Add("Username", "testuser");

            // Act
            HttpResponseMessage response = new HttpResponseMessage();
            using (var file1 = File.OpenRead(@"kivanc_bilen.pdf"))
            using (var content1 = new StreamContent(file1))
            using (var formData = new MultipartFormDataContent())
            {
                // Add file (file, field name, file name)
                formData.Add(content1, "file", "kivanc_bilen.pdf");
                response = await Client.PostAsync(request, formData);
            }

            // Assert
            Assert.Equal("401",Convert.ToInt32(response.StatusCode).ToString());
            
            response.Dispose();
            //Client.Dispose();
        }

        [Fact]
        public async Task TestDocumentDownload()
        {
            // Arrange
            var request = "/api/Document/download/1";
            Client.DefaultRequestHeaders.Add("Username", "kivanc");

            // Act
            var response = await Client.GetAsync(request);

            // Assert
            response.EnsureSuccessStatusCode();

            response.Dispose();
            //Client.Dispose();
        }
    }
}
